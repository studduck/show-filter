const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const showFilter = require('./middlewares/show-filter');
const logger = require('./middlewares/logger');
const expressWinston = require('express-winston');

if (cluster.isMaster) {
  // Fork workers.
  for(var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    logger.service.error(`worker ${worker.process.pid} died`);
  });
} else {

    function throwError(err, res) {
      logger.service.error(err.stack);
      res.status(400).json({error: 'Could not decode request: ' + err.message});
      res.end();
      return;
    };

    app.use(expressWinston.errorLogger({
      transports: [logger.fileTransport]
    }));

    app.use(bodyParser.json());

    app.post('/', function (req, res) {
        showFilter(req.body, function(err, shows) {

            if (err) {
                throwError(err, res);
                return;
            }

            res.json({response: shows});
            req.on('end', function() {
                res.end();
            });
        });
    });

    app.use(function (err, req, res, next) {
        if (err) {
            throwError(err, res)
        } else {
            next();
        }
    });

    var port = process.argv[2] ? process.argv[2] : 8000;
    app.listen(port);
}
