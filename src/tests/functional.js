var supertest = require("supertest");
var should = require("should");
var server = supertest.agent("http://localhost:8000");
var endpointPath = "/";

describe("Test filter endpoint with bad post data", function () {

    it("should return a bad request error on post with no payload", function (done) {

        server
            .post(endpointPath)
            .send({})
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                done();
            });
    });

    it("should return a bad request error on payload that is not an array", function (done) {

        server
            .post(endpointPath)
            .send({payload: "fail"})
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                done();
            });
    });

    it("should return a bad request error invalid json", function (done) {

        server
            .post(endpointPath)
            .send('{"not real json"}')
            .type('json')
            .expect("Content-type", /json/)
            .expect(400)
            .end(function (err, res) {
                res.status.should.equal(400);
                done();
            });
    });
});

describe("Test filter endpoint with valid data", function () {

    it("should return filtered data", function (done) {

        var data = {
            payload: [
                {
                    drm: true,
                    episodeCount: 3,
                    image: {
                        showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg"
                    },
                    slug: "show/test",
                    title: "test"
                },
                {
                    drm: true,
                    episodeCount: 5,
                    slug: "show/test2",
                    title: "test2"
                },
                ['array shouldnt work'],
                'string shouldnt work',
                0
            ]
        };

        var expectedResult = {
            response: [
                {
                    image: 'http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg',
                    slug: 'show/test',
                    title: 'test'
                },
                {
                    slug: 'show/test2',
                    title: 'test2'
                }
            ]
        };

        server
            .post(endpointPath)
            .send(data)
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                JSON.stringify(res.body).should.equal(JSON.stringify(expectedResult));
                done();
            });
    });

    it("should not return shows with 0 episodes", function (done) {

        var data = {
            payload: [
                {
                    drm: true,
                    episodeCount: 0,
                    image: {
                        showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg"
                    },
                    slug: "show/test",
                    title: "test"
                }
            ]
        };

        server
            .post(endpointPath)
            .send(data)
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                JSON.stringify(res.body).should.equal(JSON.stringify({response: []}));
                done();
            });
    });

    it("should not return shows were drm is false", function (done) {

        var data = {
            payload: [
                {
                    drm: false,
                    episodeCount: 5,
                    image: {
                        showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg"
                    },
                    slug: "show/test",
                    title: "test"
                }
            ]
        };

        server
            .post(endpointPath)
            .send(data)
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.status.should.equal(200);
                JSON.stringify(res.body).should.equal(JSON.stringify({response: []}));
                done();
            });
    });
});