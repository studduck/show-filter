var Show = function (image, slug, title) {
    "use strict";

    this.image = image;
    this.slug = slug;
    this.title = title;
};

module.exports = Show;