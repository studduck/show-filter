const Show = require(process.cwd() + '/models/show.js');
const logger = require(process.cwd() + '/middlewares/logger');

function validateShow(show) {
    "use strict";

    var requiredFields = ['image', 'slug', 'title'];

    requiredFields.forEach(function (requiredField) {
        if (show[requiredField] === undefined) {
            logger.service.info('Show: ' + JSON.stringify(show) + ' missing ' + requiredField + '.');
        }
    });
}

module.exports = function (data, callback) {
    "use strict";

    var err = null;
    var payload = data.payload;
    var shows = [];

    if (!payload) {
        err = new Error('payload missing from request body.');
    } else if (!Array.isArray(payload)) {
        err = new Error('payload is malformed, array expected.');
    } else {
        payload.forEach(function (showData) {
            if (showData.drm && Number(showData.episodeCount) > 0) {
                var image = undefined;
                var image_parent = showData.image;
                if (typeof image_parent === 'object' && image_parent.showImage !== undefined) {
                    image = image_parent.showImage;
                }
                var show = new Show(image, showData.slug, showData.title);
                validateShow(show);
                shows.push(show);
            }
        });
    }
    callback(err, shows);
};