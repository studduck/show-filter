const winston = require('winston');

winston.add(winston.transports.File, {filename: 'app/logs/app.log'});

module.exports.service = winston;
module.exports.fileTransport = new winston.transports.File(
    {filename: 'app/logs/app.log', maxsize: 1024 * 1024 * 10, maxFiles: 5}
);