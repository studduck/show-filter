#! /bin/bash

# System
apt-get install -y git acl ruby2.0 wget
curl -O https://bootstrap.pypa.io/get-pip.py
python get-pip.py
pip install awscli

# Update DNS
IP_ADDRESS="$(aws ec2 describe-instances --query "Reservations[*].Instances[*].PublicIpAddress" --output=text --region=ap-southeast-2)"
TEMP_FILE_LOCATION="/tmp/update_ip.json"
ZONE_ID="Z3BFV7JDY7BYZR"
echo '{"Comment":"Update record to reflect new IP address current instance.","Changes":[{"Action":"UPSERT","ResourceRecordSet":{"Name":"show-filter.joshuacullen.com.","Type":"A","TTL":300,"ResourceRecords":[{"Value":'"\"$IP_ADDRESS\""'}]}}]}' > $TEMP_FILE_LOCATION
aws route53 change-resource-record-sets --hosted-zone-id $ZONE_ID --change-batch file://$TEMP_FILE_LOCATION

# Node
curl -sL https://deb.nodesource.com/setup_6.x | bash -
apt-get install -y nodejs
apt-get install -y build-essential
npm install pm2 -g

# Nginx
apt-get install nginx -y
rm -f /etc/nginx/sites-enabled/default
aws s3 cp s3://show-filter-util/show-filter.joshuacullen.com.conf /etc/nginx/sites-enabled/show-filter.joshuacullen.com.conf --region ap-southeast-2
service nginx reload

# Project and permissions
DEPLOY_USER="deployer"
WEB_SERVER_USER="www-data"
PROJECT_ROOT="/var/projects"

id -u $DEPLOY_USER &>/dev/null || useradd -m $DEPLOY_USER 
mkdir -p $PROJECT_ROOT
chown $DEPLOY_USER:$DEPLOY_USER $PROJECT_ROOT

setfacl -R -m u:$WEB_SERVER_USER:rwX -m u:$DEPLOY_USER:rwX $PROJECT_ROOT
setfacl -dR -m u:$WEB_SERVER_USER:rwX -m u:$DEPLOY_USER:rwX $PROJECT_ROOT

# Codedeploy
cd /home/ubuntu
wget https://aws-codedeploy-ap-southeast-2.s3.amazonaws.com/latest/install
chmod +x ./install
./install auto