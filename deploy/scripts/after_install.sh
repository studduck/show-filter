#!/bin/bash

DEPLOYER_USER="deployer"
PROJECT_ROOT="/var/projects"

chown -R $DEPLOYER_USER:$DEPLOYER_USER $PROJECT_ROOT

cd $PROJECT_ROOT/show-filter/src/
sudo -u $DEPLOYER_USER npm install
