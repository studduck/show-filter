#!/bin/bash

DEPLOYER_USER="deployer"

cd /var/projects/show-filter/src/
sudo -u $DEPLOYER_USER pm2 start index.js

service nginx restart
