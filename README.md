Show filter
========================

## Building the app

The app code is in the **src** directory.
The app requires NodeJS and NPM.

To install the dependencies:

```
npm install
```

To run the app:

```
node index.json <port>
```

## Testing the app

Tests are in the **src/tests** directory.

To run the tests:

```
npm test
```

## Deploying the app

Everything to deploy the app into AWS is contained in the deploy directory.

**deploy/config/prod** contains the nginx config and the vapour yaml file.

**deploy/scripts** contains the CodeDeploy scripts used for deployments.

**deploy/templates/prod** contains the CloudFormation template to build the required resources and the cloud-init.sh script used when instances are launched.